/*
 * Graph.cpp
 *
 *  Created on: Nov 10, 2016
 *      Author: jgoeders
 */

#include "NIGraph.h"

#include <string>
#include <sstream>
#include <cassert>




using namespace std;

NIGraph::NIGraph() {
	// TODO Auto-generated constructor stub

}

NIGraph::~NIGraph() {
	// TODO Auto-generated destructor stub
}

void NIGraph::addNode(std::unique_ptr<NIGraphNode> node) {

	string id = node->getId();
	assert(idMapNode.find(id) == idMapNode.end());

	idMapNode[id] = node.get();

	nodes.push_back(move(node));

}

void NIGraph::addEdge(std::unique_ptr<NIGraphEdge> edge) {
	string id = edge->getId();
	assert(idMapEdge.find(id) == idMapEdge.end());

	idMapEdge[id] = edge.get();

	edge->getSourceNode()->addOutEdge(edge.get());
	edge->getDestNode()->addInEdge(edge.get());

	edges.push_back(move(edge));
}

std::string NIGraph::stats() {
	stringstream s;

	s << "Number of nodes: " << getNumNodes() << endl;

	int numOutEdges = 0;
	int numInEdges = 0;
	for (auto &node : nodes) {
		numOutEdges += node->getOutEdges().size();
		numInEdges += node->getInEdges().size();
	}
	s << "Avg num out edges: " << (numOutEdges / (float) getNumNodes()) << endl;
	s << "Avg num in edges: " << (numInEdges / (float) getNumNodes()) << endl;

	return s.str();

}

NIGraphNode * NIGraph::findNodeById(std::string id) {
	assert(idMapNode.find(id) != idMapNode.end());

	return idMapNode[id];
}
