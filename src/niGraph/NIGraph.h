/*
 * Graph.h
 *
 *  Created on: Nov 10, 2016
 *      Author: jgoeders
 */

#ifndef SRC_GRAPH_GRAPH_H_
#define SRC_GRAPH_GRAPH_H_

#include <vector>
#include <memory>
#include <map>

#include "NIGraphEdge.h"
#include "NIGraphNode.h"

class NIGraph {
private:
	std::vector<std::unique_ptr<NIGraphNode>> nodes;
	std::map<std::string, NIGraphNode*> idMapNode;

	std::vector<std::unique_ptr<NIGraphEdge>> edges;
	std::map<std::string, NIGraphEdge*> idMapEdge;

public:
	NIGraph();
	virtual ~NIGraph();

	void addNode(std::unique_ptr<NIGraphNode>);
	void addEdge(std::unique_ptr<NIGraphEdge>);

	std::string stats();

	int getNumNodes() { return nodes.size(); }
	NIGraphNode * findNodeById(std::string id);

	std::vector<std::unique_ptr<NIGraphEdge>> const& getEdges() const { return edges; }
	std::vector<std::unique_ptr<NIGraphNode>> const& getNodes() const { return nodes; }
};

#endif /* SRC_GRAPH_GRAPH_H_ */
