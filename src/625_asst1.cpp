/*
 * 522r_asst1.cpp
 *
 *  Created on: Jan 6, 2017
 *      Author: jgoeders
 */

#include <regex>
#include <vector>
#include <deque>
#include <map>
#include <iostream>
#include <cstdlib>

#include "common/utils.h"
#include "niGraph/NIGraph.h"
#include "niGraphReader/NIGraphReader.h"

using namespace std;

// Print a list of nodes
void printNodeList(const deque<NIGraphNode*> & nodes) {
	int i = 0;
	for (auto nodeIt = nodes.begin(); nodeIt != nodes.end(); nodeIt++, i++) {
		NIGraphNode * node = *nodeIt;
		if (nodeIt != nodes.begin()) {
			cout << " -> ";
			if (i % 10 == 0)
				cout << "\n";
		}
		cout << node->getId();

	}
	cout << "\n";
}

// Create a DOT file of the graph
// - graph: The graph to output in DOT format
// - outputPath: The path of the output file
void createDOT(const NIGraph & graph, const string outputPath) {
	// add code here
}

// Perform a topological sort of the graph
// graph - Input graph
// Return value - List of nodes sorted in topological order
deque<NIGraphNode*> topologicalSort(const NIGraph & graph) {
	// add code here
}

// Find the longest delay path based on a topological sorting of nodes
// topolSortedNodes - List of nodes, sorted in topological order
// longestPath - Return value, list of nodes on longest path, in topological order
// Return value - Total delay value of longest path
int longestDelayPath(const deque<NIGraphNode*> & topolSortedNodes,
		deque<NIGraphNode*> & longestPath) {
	// add code here
}

int main(int argc, char ** argv) {
	// Get list of graphs in graph directory
	string graphsGlob = "../niGraphs/DelayGraph_*.graphml";
	vector<string> graphPaths = glob(graphsGlob);

	// Check that graphs were found
	if (graphPaths.size() == 0) {
		cout << "No graphs found in specified directory\n";
		exit(1);
	}

	// Store graphs in a map by graph #
	// Use a regex to extract the graph # from the path string
	// A map structure will automatically keep the values sorted by key
	map<int, string> graphPathsById;
	for (auto graphPath : graphPaths) {
		regex regex_str(".*DelayGraph_(\\d+)\\.graphml");
		smatch matches;

		regex_match(graphPath, matches, regex_str);
		assert(matches.size() == 2);
		int graphId = atoi(matches[1].str().c_str());
		graphPathsById[graphId] = graphPath;
	}

	// Create graph reader object
	NIGraphReader graphReader;

	// Parse graph 0, and create a DOT file
	unique_ptr<NIGraph> graph0 = graphReader.parseGraphMlFile(
			graphPathsById[0]);
	createDOT(*(graph0.get()), "graph0.dot");


	// Open a file to write results out to
	ofstream fp;
	fp.open("results.txt");

	// Loop through the graphs
	for (auto graphPathIt : graphPathsById) {
		auto graphPath = graphPathIt.second;

		// Print which graph we are working on
		cout << graphPath << "\n";

		// Parse the graph
		unique_ptr<NIGraph> graph = graphReader.parseGraphMlFile(graphPath);

		// Time the topological sort
		timestamp_t t0 = get_timestamp();
		deque<NIGraphNode*> sortedNodes = topologicalSort(*(graph.get()));
		timestamp_t t1 = get_timestamp();

		// Find the longest path
		deque<NIGraphNode*> longestPath;
		int delay = longestDelayPath(sortedNodes, longestPath);

		// Write the results out to the file
		int graphSize = graph->getNodes().size() + graph->getEdges().size();
		fp << graphPath << " " << graphSize << " "
				<< (t1 - t0) / MICROSECS_PER_SEC << " " << delay << "\n";

		//printNodeList(longestPath);
	}

	// Close the file
	fp.close();

}
