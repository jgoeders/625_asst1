file(GLOB SOURCES *.cpp *.h)

add_library(common ${SOURCES})

target_include_directories(common PUBLIC ".")
